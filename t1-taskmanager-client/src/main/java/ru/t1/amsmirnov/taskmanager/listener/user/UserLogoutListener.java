package ru.t1.amsmirnov.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLogoutRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLogoutResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    public static final String DESCRIPTION = "Logout from TaskDTO Manager.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userLogoutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        @NotNull final UserLogoutResponse response = serviceLocator.getAuthEndpoint().logout(request);
        setToken(null);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
