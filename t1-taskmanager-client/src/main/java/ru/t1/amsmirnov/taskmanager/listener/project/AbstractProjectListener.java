package ru.t1.amsmirnov.taskmanager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IProjectEndpoint;
import ru.t1.amsmirnov.taskmanager.listener.AbstractListener;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    @Autowired
    public IProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@NotNull final ProjectDTO project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}
