package ru.t1.amsmirnov.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.listener.AbstractListener;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected AbstractListener[] listeners;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    public boolean isSystemCommand(@Nullable final String name) {
        if (name == null || name.isEmpty()) return false;
        return name.equals(this.getName()) || name.equals(this.getArgument());
    }

}
