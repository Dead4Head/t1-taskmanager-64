package ru.t1.amsmirnov.taskmanager.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataJsonLoadFasterXMLRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataJsonLoadFasterXMLResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class DataJsonLoadFasterXMLListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-json";

    @NotNull
    public static final String DESCRIPTION = "Load data from JSON file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataJsonLoadFasterXMLListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataJsonLoadFasterXMLRequest request = new DataJsonLoadFasterXMLRequest(getToken());
        @NotNull final DataJsonLoadFasterXMLResponse response = domainEndpoint.loadDataJsonFasterXML(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
