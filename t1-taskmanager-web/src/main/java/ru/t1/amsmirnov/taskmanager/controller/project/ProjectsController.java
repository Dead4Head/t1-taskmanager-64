package ru.t1.amsmirnov.taskmanager.controller.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.amsmirnov.taskmanager.repository.ProjectWebRepository;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectWebRepository projectRepository;

    @GetMapping("/projects")
    public ModelAndView projects() {
        return new ModelAndView("project-list", "projects", projectRepository.findAll());
    }

}
