package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.amsmirnov.taskmanager.model.TaskWeb;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public final class TaskWebRepository {

    @NotNull
    private static final TaskWebRepository INSTANCE = new TaskWebRepository();

    public static TaskWebRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final Map<String, TaskWeb> tasks = new LinkedHashMap<>();

    {
        add(new TaskWeb("First_Task"));
        add(new TaskWeb("Second_Task"));
        add(new TaskWeb("Third_Task"));
    }

    public void add(@NotNull final TaskWeb task) {
        tasks.put(task.getId(), task);
    }

    public void create() {
        add(new TaskWeb("New Task " + System.currentTimeMillis()));
    }

    @NotNull
    public Collection<TaskWeb> findAll() {
        return tasks.values();
    }

    @Nullable
    public TaskWeb findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void save(@NotNull final TaskWeb task) {
        tasks.put(task.getId(), task);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void removeAll() {
        tasks.clear();
    }

}
