<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp" />
<h1>TASK LIST</h1>
<table width="100%" cellpadding="10" border="1">
    <tr>
        <th width="200" align="center" nowrap="nowrap">ID</th>
        <th width="100%" align="center" nowrap="nowrap">PROJECT</th>
        <th width="200" align="center" nowrap="nowrap" align="left">NAME</th>
        <th width="100%" align="center" nowrap="nowrap">DESCRIPTION</th>
        <th width="150" align="center" nowrap="nowrap">STATUS</th>
        <th width="100" align="center" nowrap="nowrap">START</th>
        <th width="100" align="center" nowrap="nowrap">FINISH</th>
        <th width="100" align="center" nowrap="nowrap">EDIT</th>
        <th width="100" align="center" nowrap="nowrap">DELETE</th>
    </tr>

    <c:forEach var="task" items="${tasks}" >
        <tr>
            <td>
                <c:out value="${task.id}" />
            </td>
            <td>
                <c:out value="${projectRepository.findById(task.projectId).name}" />
            </td>
            <td>
                <c:out value="${task.name}" />
            </td>
            <td>
                <c:out value="${task.description}" />
            </td>
            <td align="center" nowrap="nowrap">
                <c:out value="${task.status.displayName}" />
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateStart}" />
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateFinish}" />
            </td>
            <td align="center">
                <a href="/task/edit/${task.id}">EDIT</a>
            </td>
            <td align="center">
                <form action="/task/delete/${task.id}" method="POST">
                    <button>DELETE</button>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" style="margin-top: 20px;" method="POST">
    <button>CREATE TASK</button>
</form>

<jsp:include page="../include/_footer.jsp" />
