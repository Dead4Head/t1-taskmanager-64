package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;

public interface IAuthService {

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws AbstractException;

    void logout(@Nullable SessionDTO session) throws AbstractException;

    @NotNull
    SessionDTO validateToken(@Nullable String token) throws AbstractException;

}
