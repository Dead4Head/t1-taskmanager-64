package ru.t1.amsmirnov.taskmanager.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public abstract class AbstractProjectResponse extends AbstractResultResponse {

    @Nullable
    private ProjectDTO project;

    protected AbstractProjectResponse() {
    }

    protected AbstractProjectResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }

    protected AbstractProjectResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

    @Nullable
    public ProjectDTO getProject() {
        return project;
    }

    public void setProject(@Nullable final ProjectDTO project) {
        this.project = project;
    }

}
