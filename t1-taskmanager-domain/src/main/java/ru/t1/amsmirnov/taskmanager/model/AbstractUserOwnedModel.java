package ru.t1.amsmirnov.taskmanager.model;

import org.jetbrains.annotations.Nullable;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    @ManyToOne
    protected User user;

    public AbstractUserOwnedModel() {

    }

    @Nullable
    public User getUser() {
        return user;
    }

    public void setUser(@Nullable final User user) {
        this.user = user;
    }

}
