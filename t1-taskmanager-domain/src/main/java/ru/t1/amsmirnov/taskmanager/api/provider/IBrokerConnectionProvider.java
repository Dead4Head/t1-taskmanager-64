package ru.t1.amsmirnov.taskmanager.api.provider;

import org.jetbrains.annotations.NotNull;

public interface IBrokerConnectionProvider {

    @NotNull
    Integer getBrokerPort();

    @NotNull
    String getBrokerHost();

}
